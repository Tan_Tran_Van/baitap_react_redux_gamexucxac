import React, { Component } from "react";
import { connect } from "react-redux";
import styles from "./gameXucXac.module.css";
import TableXucXac from "./TableXucXac";

class GameXucXac extends Component {
  render() {
    return (
      <div className={styles.fontXucXac}>
        <div className="container p-5">
          <h1>GAME XÚC SẮC</h1>
          <TableXucXac />
          <h2>BẠN CHỌN: {this.props.choseTaiXiu}</h2>
          <h3>Tổng số lần Bạn Thắng: {this.props.totalWin}</h3>
          <h3>Tổng số lần Tung Xúc Xắc: {this.props.totalTungXucXac}</h3>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    choseTaiXiu: state.GameXucXacReducer.choseTaiXiu,
    totalWin: state.GameXucXacReducer.totalWin,
    totalTungXucXac: state.GameXucXacReducer.totalTungXucXac,
  };
};
export default connect(mapStateToProps, null)(GameXucXac);
