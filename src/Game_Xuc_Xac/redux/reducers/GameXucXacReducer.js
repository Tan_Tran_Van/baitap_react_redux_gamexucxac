import { CHOSE_TAI_XIU, PLAY_GAME } from "../constants/XucXacConstant";

const dataXucXac = [
  {
    id: 1,
    img: "./image_Game_XucXac/1.png",
  },
  {
    id: 2,
    img: "./image_Game_XucXac/2.png",
  },
  {
    id: 3,
    img: "./image_Game_XucXac/3.png",
  },
  {
    id: 4,
    img: "./image_Game_XucXac/4.png",
  },
  {
    id: 5,
    img: "./image_Game_XucXac/5.png",
  },
  {
    id: 6,
    img: "./image_Game_XucXac/6.png",
  },
];

let initialState = {
  dataXucXac: dataXucXac,
  detailXucXac: [1, 2, 3],
  totalWin: 0,
  thongBaoKetQua: "-",
  totalTungXucXac: 0,
  choseTaiXiu: "-",
};

export const GameXucXacReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHOSE_TAI_XIU: {
      if (action.taiXiu) {
        state.choseTaiXiu = "TÀI";
      } else {
        state.choseTaiXiu = "XỈU";
      }
      return { ...state };
    }
    case PLAY_GAME: {
      state.totalTungXucXac++;
      let randomNo1 = Math.floor(Math.random() * 6) + 1;
      let randomNo2 = Math.floor(Math.random() * 6) + 1;
      let randomNo3 = Math.floor(Math.random() * 6) + 1;
      state.detailXucXac = [randomNo1, randomNo2, randomNo3];
      let sumNumber = randomNo1 + randomNo2 + randomNo3;
      let ketQuaTaiXiu = "";
      if (sumNumber > 11) {
        ketQuaTaiXiu = "TÀI";
      } else {
        ketQuaTaiXiu = "XỈU";
      }
      if (state.choseTaiXiu === ketQuaTaiXiu) {
        state.totalWin++;
        state.thongBaoKetQua = "YOU WIN";
      } else {
        state.thongBaoKetQua = "YOU LOSE";
      }
      return { ...state };
    }
    default:
      return { ...state };
  }
};
