import React, { Component } from "react";
import { connect } from "react-redux";
import { CHOSE_TAI_XIU, PLAY_GAME } from "./redux/constants/XucXacConstant";

class TableXucXac extends Component {
  renderImageXucXac = () => {
    return this.props.detailXucXac.map((item, index) => {
      let indexdataXucXac = this.props.dataXucXac.findIndex((xucXac) => {
        return xucXac.id === item;
      });
      return (
        <img
          className="w-25 m-1"
          src={this.props.dataXucXac[indexdataXucXac].img}
          alt=""
        />
      );
    });
  };
  render() {
    return (
      <div className="row p-5 text-center">
        <div className="col-4">
          <button
            className="btn btn-danger fs-1 p-5"
            onClick={() => {
              this.props.handleChoseTaiXiu(false);
            }}
          >
            XỈU
          </button>
        </div>
        <div className="col-4 align-items-center">
          {this.renderImageXucXac()}
        </div>
        <div className="col-4">
          <button
            className="btn btn-danger fs-1 p-5"
            onClick={() => {
              this.props.handleChoseTaiXiu(true);
            }}
          >
            TÀI
          </button>
        </div>
        <div className="offset-4 col-4 offset-4">
          <button
            className="btn btn-danger fs-4 m-2"
            onClick={() => {
              this.props.handlePlayGame(true);
            }}
          >
            PLAY GAME
          </button>
        </div>
        <div className="col-12 fs-2 text-danger">
          {this.props.thongBaoKetQua}
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    detailXucXac: state.GameXucXacReducer.detailXucXac,
    dataXucXac: state.GameXucXacReducer.dataXucXac,
    thongBaoKetQua: state.GameXucXacReducer.thongBaoKetQua,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleChoseTaiXiu: (taiXiu) => {
      let action = {
        type: CHOSE_TAI_XIU,
        taiXiu,
      };
      dispatch(action);
    },
    handlePlayGame: () => {
      let action = {
        type: PLAY_GAME,
      };
      dispatch(action);
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(TableXucXac);
