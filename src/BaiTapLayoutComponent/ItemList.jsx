import React, { Component } from "react";
import Item from "./Item";

export default class ItemList extends Component {
  render() {
    return (
      <div className="row p-4 px-lg-5">
        <Item />
        <Item />
        <Item />
        <Item />
        <Item />
        <Item />
      </div>
    );
  }
}
